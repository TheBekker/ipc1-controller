﻿using System;
using System.IO;
using System.Text;
using System.Threading;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using GHIElectronics.NETMF.FEZ;
using GHIElectronics.NETMF.Net;
using GHIElectronics.NETMF.Net.NetworkInformation;
using Socket = GHIElectronics.NETMF.Net.Sockets.Socket;
using GHIElectronics.NETMF.Net.Sockets;
using IPC1 = IPC1NetControl.IPC1NetControl;

namespace Controller {
    public class Program {
        public static void Main() {
            byte[] ip = { 192, 168, 1, 222};
            IPC1.initW5100(ip);
            IPC1.DecoderControl(IPC1.DecoderCmd.Center);
        }
    }
}
